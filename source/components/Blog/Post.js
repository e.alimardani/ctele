import React from 'react';
import {ActivityIndicator, Text, View, StatusBar, WebView, Image, Platform} from 'react-native';

import {Container, Header, Left, Body, Right, Content} from 'native-base';
import {styles} from './Style';
import BackButton from "../Shared/BackButton";
import Helpers from "../Shared/Helper";
import HTML from 'react-native-render-html';

export default class Post extends React.Component {

    constructor(props)
    {
        super(props);

        this.state = {
            url: '',
            loading: false
        };

        this.post_desc = props.post.post_desc !== null ? "<div style='font-family: Vazir;font-size:12px;direction: rtl;text-align: right;'>" + props.post.post_desc + "</div>" : "<div style='font-family: Vazir;font-size:12px;direction: rtl;text-align: right;'>توضیحی ثبت نشده است!</div>"; ;

        console.log(props);

        this.renderImage = this.renderImage.bind(this);
    }

    async componentWillMount(){
        if(await Helpers.CheckNetInfo()){
            return;
        }
        this.setState({
            url: this.props.url,
        })
    }

    renderImage(prop){
        let width = prop.width && prop.width !== null ? parseInt(prop.width) : 0;
        let height = prop.height && prop.height !== null ? parseInt(prop.height) : 0;
        console.log(prop);
        return(
            <Image key={12} source={{uri : prop.src}} style={[styles.renderedImage,{width:width}]} />
        );
    }

    render() {


        return (
            <Container>
                <Header style={[styles.browserHeader,{overflow: 'hidden',zIndex:999}]} >
                    <Image resizeMode="cover" style={styles.browserMainBackground} source={require("./../../assets/images/shared/mainbg.jpg")} />
                    <Left style={{flex: .2,justifyContent:'center',alignItems:'flex-start'}}>
                        <BackButton/>
                    </Left>
                    <Body style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                    <Text numberOfLines={1} style={[styles.notFoundText,{marginTop:0,color:'#fff'}]}>{this.props.title}</Text>
                    </Body>
                    <Right style={{flex: .2,justifyContent:'center',alignItems:'flex-end'}} >
                        {this.state.loading && <ActivityIndicator color="#ffffff"/>}
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="dark-content" />

                {/*<WebView*/}
                {/*source={{uri: this.state.url , method : 'GET' }}*/}
                {/*onLoadStart={() => { this.setState({loading:true}) } }*/}
                {/*onLoadEnd={() => { this.setState({loading:false}) } }*/}
                {/*style={{zIndex:-1}}*/}
                {/*/>*/}
                <Content style={{flex:1,backgroundColor:'#ffffff'}}>
                    <HTML html={this.post_desc}
                          containerStyle={{margin:10}}
                          tagsStyles={{
                              p : {justifyContent:Platform.OS === 'ios' ? 'flex-end' : 'flex-start',textAlign:Platform.OS === 'ios' ? 'justify' : 'right',writingDirection:'rtl'} ,
                              div : {textAlign:Platform.OS === 'ios' ? 'justify' : 'right',writingDirection:'rtl'},
                              ul : {textAlign:Platform.OS === 'ios' ? 'justify' : 'right',writingDirection:'rtl'},
                              li : {textAlign:Platform.OS === 'ios' ? 'justify' : 'right',writingDirection:'rtl'}
                          }}
                        //imagesMaxWidth={200} imagesInitialDimensions={{width:200,height:200}}
                          renderers = {{
                              img: this.renderImage
                          }}
                    />
                </Content>

            </Container>
        )
    }
}
