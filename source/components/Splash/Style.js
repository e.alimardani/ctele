import EStyleSheet from 'react-native-extended-stylesheet';
import {Platform,Dimensions} from "react-native";

export const styles = EStyleSheet.create({
    mainContainer : {
        flex:1 ,
        flexDirection : 'column',
        justifyContent: 'center' ,
        alignItems: 'center',
        backgroundColor:'$MainColor'
    },
    mainBackground:{
        position: 'absolute',
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
        //opacity:0.7
    },
    splashMainSection:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -50
    },
    splashImage:{
        width:250,
        height:100,
        backgroundColor:'transparent',
        alignSelf:'center',
        resizeMode:'contain'
    },
    splashSlogan:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:12,
        color:'$MainFontColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'bold'
            },
            android: {
                fontFamily: '$YekanBold'
            }
        }),
        marginTop:15
    },
    splashBottomSection:{
        backgroundColor:'transparent',
        position:'absolute' ,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height * 0.3,
        bottom:0,
        justifyContent:'center',
        alignItems:'center'
    },
    button:{
        backgroundColor:'$ButtonBackgroundColor',
        width:200,
        height:40,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5,
        ...Platform.select({
            ios: {
                shadowColor: 'rgb(0, 0, 0)',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.2,
                shadowRadius: 5,
            },
            android: {
                elevation: 2
            }
        }),
    },
    buttonText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        //marginTop:-5,
        //lineHeight:16,
        color:'$ButtonFontColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: '$YekanRegular',
            }
        }),
    },
    buttonTransparentText:{
        textAlign:'center',
        writingDirection:'rtl',
        fontSize:14,
        //lineHeight:14,
        color:'$MainFontColor',
        ...Platform.select({
            ios: {
                fontFamily: '$YekanRegular',
                fontWeight: 'normal'
            },
            android: {
                fontFamily: '$YekanRegular'
            }
        }),
    }
});


export default styles;
