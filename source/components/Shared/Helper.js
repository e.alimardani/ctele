import React from 'react';
import {Alert, NetInfo} from "react-native";
import {Actions} from "react-native-router-flux";

const Helpers  = {
    NumberFormat: (number) => {
        let digitCount = (number+"").length;
        let formatedNumber = number+"";
        let ind = digitCount%3 || 3;
        let temparr = formatedNumber.split('');

        if( digitCount > 3 && digitCount <= 6 ){

            temparr.splice(ind,0,',');
            formatedNumber = temparr.join('');

        }else if (digitCount >= 7 && digitCount <= 15) {
            let temparr2 = temparr.slice(0, ind);
            temparr2.push(',');
            temparr2.push(temparr[ind]);
            temparr2.push(temparr[ind + 1]);

            if (digitCount >= 7 && digitCount <= 9) {
                temparr2.push(" میلیون");
            } else if (digitCount >= 10 && digitCount <= 12) {
                temparr2.push(" میلیارد");
            } else if (digitCount >= 13 && digitCount <= 15) {
                temparr2.push(" تیلیارد");

            }
            formatedNumber = temparr2.join('');
        }
        return formatedNumber;
    },
    ToPersianNumber: (input) => {

        if(input !== "" && input !== null && typeof input !== typeof undefined)
        {
            let persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

            for(let i=0;i<10;i++){
                let regex=eval("/"+i+"/g");
                input = input.replace(regex,persian[i]);

            }
            return input;
        }
        else {
            return "";
        }

    },
    ToEnglishNumber: (input) => {

        if(input !== "" && input !== null && typeof input !== typeof undefined)
        {
            //let english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

            input = input.replace('۰','0');
            input = input.replace('۱','1');
            input = input.replace('۲','2');
            input = input.replace('۳','3');
            input = input.replace('۴','4');
            input = input.replace('۵','5');
            input = input.replace('۶','6');
            input = input.replace('۷','7');
            input = input.replace('۸','8');
            input = input.replace('۹','9');

            return input;
        }
        else {
            return "";
        }

    },
    RedirectToLogin:() => {
        Alert.alert(
            'اعتبار ورود شما منقضی شده است.',
            'لطفا مجددا وارد شوید.',
            [
                {text: 'ورود مجدد', onPress: () => Actions.reset('login')}
            ],
            { cancelable: false },
            'default'
        )
    },
    CheckNetInfo:async () => {

        let conInfo = await NetInfo.getConnectionInfo();

        if(conInfo.type === 'none')
        {
            Alert.alert(
                'ارتباط اینترنت شما قطع میباشد!',
                'لطفا تنظیمات اینترنت خود را چک نمایید.',
                [
                    {text: 'تایید'},
                ]
            );
            return true;
        }
        else {
            return false;
        }
    },
    ReturnStatus:(item) =>{
        switch (item) {
            case 'pending':
                return 'در انتظار پرداخت';
            case 'processing':
                return 'در حال انجام';
            case 'on-hold':
                return 'در انتظار بررسی';
            case 'completed':
                return 'تکمیل شده';
            case 'cancelled':
                return 'لغو شده';
            case 'refunded':
                return 'مسترد شده';
            case 'failed':
                return 'نا موفق';
        }
    },
    ReturnStatusColor:(item) =>{
        switch (item) {
            case 'pending':
                return '#ff9f43';
            case 'processing':
                return '#1fc792';
            case 'on-hold':
                return '#ff9f43';
            case 'completed':
                return '#1fc792';
            case 'cancelled':
                return '#ff4141';
            case 'refunded':
                return '#ff4141';
            case 'failed':
                return '#ff4141';
        }
    },
    ReturnStatusIcon:(item) =>{
        switch (item) {
            case 'pending':
                return 'md-card';
            case 'processing':
                return 'md-checkmark-circle';
            case 'on-hold':
                return 'md-cloud-circle';
            case 'completed':
                return 'md-checkmark-circle';
            case 'cancelled':
                return 'md-close-circle';
            case 'refunded':
                return 'md-close-circle';
            case 'failed':
                return 'md-close-circle';
        }
    },
    ReturnFileIconName:(item) =>{
        switch (item) {
            case 'audio/mpeg':
                return 'md-mic';
            case 'video/mp4':
                return 'md-film';
            case 'application/pdf':
                return 'md-book';
            default:
                return 'md-document';
        }
    },
};

export default Helpers;