import React from 'react';
import {
    StatusBar,
    Image,
    Text,
    TouchableOpacity,
    View,
    AsyncStorage,
    ActivityIndicator,
    FlatList,
    PixelRatio,
    Linking,
    Platform,
    ScrollView
} from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setApp} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import BasketButton from "../Shared/BasketButton";
import BackButton from "../Shared/BackButton";
import SearchButton from "../Shared/SearchButton";
import LinearGradient from "react-native-linear-gradient";
import Swiper from "react-native-swiper";
//import ViewMoreText from 'react-native-view-more-text';
import NumberFormat from "react-number-format";
import Helpers from "../Shared/Helper";
import LoadingModal from "../Shared/LoadingModal";
import Picker from "react-native-picker";
import Modal from "react-native-modal";
import * as Progress from 'react-native-progress';
import HTML from 'react-native-render-html';

class Product extends React.Component {

    constructor(props) {
        super(props);

        this.newDate = props.global.newDate;

        this.state = {
            showLoadingModal:false,
            loading:true,
            downloadable:null,
            isBuy:null,
            files:[],
            desc:null,
            count:props.app.cart.findIndex(a => a.id === props.product.id) === -1 ? 0 : props.app.cart.find(a => a.id === props.product.id).count,
            //Files
            showFileModal:false,
            source:'',
            fileName:'',
            posterSource:props.product.image !== null ? props.product.image : '',
            hasLocale:false,
            downloadPercent:0,
            downloading:false,
            totalSize:0,
            downloadedSize:0,
            canceled:false,
        };

        let images = [];

        if(props.product.image !== null){
            images.push(props.product.image);
        }
        if(props.product.gallery !== null && props.product.gallery.length > 0){
            images.push(...props.product.gallery);
        }

        this.images = images;

        this.selectedFile = null;

        this.addToCart = this.addToCart.bind(this);
        this.deleteFromCart = this.deleteFromCart.bind(this);
        this.updateCart = this.updateCart.bind(this);
        this.showPicker = this.showPicker.bind(this);

        this.addFavorite = this.addFavorite.bind(this);
        this.deleteFavorite = this.deleteFavorite.bind(this);

        this.getProductRequest = this.getProductRequest.bind(this);

        //Files
        this.showFile = this.showFile.bind(this);
        this.view = this.view.bind(this);
        this.downloadFile = this.downloadFile.bind(this);
        this.cancelDownload = this.cancelDownload.bind(this);
        this.deleteFile = this.deleteFile.bind(this);

        this.renderImage = this.renderImage.bind(this);

    }

    async componentWillMount(){
        await this.getProductRequest();
    }

    async componentWillReceiveProps(newProps){

        await this.setState({
            count:newProps.app.cart.findIndex(a => a.id === this.props.product.id) === -1 ? 0 : newProps.app.cart.find(a => a.id === this.props.product.id).count
        });

    }

    renderViewMore(onPress){
        return(
            <View style={styles.productDescInfoViewMore}>
                <Text style={styles.productDescInfoViewMoreText} onPress={onPress}>نمایش بیشتر</Text>
                <Icon style={styles.productDescInfoViewMoreIcon} name='md-add-circle'/>
            </View>
        )
    };
    renderViewLess(onPress){
        return(
            <View style={styles.productDescInfoViewMore}>
                <Text style={styles.productDescInfoViewMoreText} onPress={onPress}>نمایش کمتر</Text>
                <Icon style={styles.productDescInfoViewMoreIcon} name='md-remove-circle'/>
            </View>
        )
    };

    renderItem({ item,index }) {
        return (
            <TouchableOpacity style={[styles.productHorizontalSectionListItem,{marginRight:index === 0 ? 15 : 15,marginLeft:index + 1 === data1.length ? 15 : 0}]} key={item.id} activeOpacity={.8}>
                <View style={styles.productHorizontalSectionListItemContainer}>
                    <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={{uri:item.image}}/>
                    {item.oldPrice !== '' && <View style={styles.productHorizontalSectionListItemPercent}>
                        <Text style={styles.productHorizontalSectionListItemPercentText}>{item.percent}</Text>
                    </View>}
                </View>
                <Text style={styles.productHorizontalSectionListItemName} numberOfLines={2}>{item.name}</Text>
                {item.oldPrice !== '' && <Text style={styles.productHorizontalSectionListItemOldPrice}>{item.oldPrice} تومان</Text>}
                <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:item.oldPrice === '' ? 10 : 0}]}>{item.price} تومان</Text>
            </TouchableOpacity>
        );
    }

    renderFileItem({ item,index }) {
        return (
            <View style={[styles.filesListItem,{marginTop:index === 0 ? 0 : 5,marginBottom:index + 1 === this.state.files.length ? 0 : 0,borderBottomWidth: index + 1 === this.state.files.length ? 0 : 5/PixelRatio.get()}]}>
                <TouchableOpacity activeOpacity={.7} style={styles.favoriteEditButton} onPress={() => this.showFile(item)}>
                    <Text style={styles.fileShowButtonText}>مشاهده</Text>
                </TouchableOpacity>
                <View style={{flexDirection:'row'}}>
                    <Text style={styles.fileListItemName} numberOfLines={2}>{item.download_name}</Text>
                    <Icon name={Helpers.ReturnFileIconName(item.mime_type.type)} style={{marginLeft:10,fontSize:26,color:this.props.global.grColorTwo}}/>
                </View>
            </View>
        );
    }
    handleFileEmpty() {
        return(
            <Text style={[styles.notFoundText,{marginTop:0}]}>فایلی وجود ندارد.</Text>
        );
    }

    //Files
    async showFile(item){
        if(this.props.user.apiToken !== null) {
            this.selectedFile = item;
            let fileName =item.id + '.' + item.mime_type.ext;
            let source = '';
            if (await AsyncStorage.getItem(fileName) !== null) {
                source = await AsyncStorage.getItem(fileName);
                await this.setState({
                    hasLocale: true,
                    fileName,
                    source,
                    downloadPercent:0,
                    downloading:false,
                    totalSize:0,
                    downloadedSize:0,
                    canceled:false,
                    showFileModal: true
                });
            }
            else {
                source = item.download_url;
                await this.setState({
                    hasLocale: false,
                    fileName,
                    source,
                    downloadPercent:0,
                    downloading:false,
                    totalSize:0,
                    downloadedSize:0,
                    canceled:false,
                    showFileModal: true
                });
            }
        }
        else {
            this.selectedFile = null;
            await this.setState({
                showFileModal:true
            });
        }
    }
    async view() {
        try {
            let item = this.selectedFile;

            await this.setState({showFileModal: false});

            setTimeout(() => {
                if(item.mime_type.ext === "mp3" || item.mime_type.ext === "mp4"){
                    if(this.state.hasLocale){
                        if(Platform.OS === "ios"){
                            const ios = RNFetchBlob.ios;
                            ios.openDocument(this.state.source);
                        }
                        else {
                            const android = RNFetchBlob.android;
                            android.actionViewIntent(this.state.source, item.mime_type.type);
                        }
                    }
                    else {
                        Linking.openURL(this.state.source);
                    }
                }
                else if(item.mime_type.ext === "pdf"){
                    Actions.push('pdfViewer', {
                        source:this.state.source,
                        title:item.download_name
                    });
                }
                else {
                    if(this.state.hasLocale){
                        if(Platform.OS === "ios"){
                            console.log(this.state.source);
                            RNFetchBlob.ios.openDocument(this.state.source);
                        }
                        else {
                            const android = RNFetchBlob.android;
                            android.actionViewIntent(this.state.source, item.mime_type.type);
                        }
                        //Linking.openURL(this.props.global.baseApiUrl);
                    }
                    else {
                        Linking.openURL(this.state.source);
                    }
                }
            },500);

        }
        catch (error) {
            console.log(error);
        }
    }
    async downloadFile() {
        let dirs = RNFetchBlob.fs.dirs;

        await this.setState({
            downloading: true,
            canceled:false,
            downloadPercent:0,
            totalSize:0,
            downloadedSize:0
        });

        this.downLoadTask = RNFetchBlob
            .config({
                fileCache: true,
                path: dirs.DocumentDir + "/" + this.state.fileName
            })
            .fetch('GET', this.state.source);


        this.downLoadTask.progress({ interval : 500 },(received, total) => {
                this.setState({
                    totalSize:total,
                    downloadedSize:Math.round((received / 1024) / 1024),
                    downloadPercent : received / total
                });
                //console.log(received,total);
            })
            .then(async (res) => {
                //console.log(res.path());
                if(!this.state.canceled)
                {
                    await AsyncStorage.setItem(this.state.fileName,res.path());
                    await this.setState({
                        source:res.path(),
                        hasLocale:true,
                        downloading:false
                    });
                }
                else{
                    this.setState({
                        downloading:false,
                        downloadPercent:0,
                        totalSize:0,
                        downloadedSize:0
                    })
                }
            })
            .catch((errorMessage) => {
                console.log(errorMessage);
            });
    }
    async cancelDownload() {
        this.downLoadTask.cancel(async (err, taskId) => {
            await this.setState({
                canceled: true,
                downloading:false,
                downloadPercent:0,
                totalSize:0,
                downloadedSize:0,
            });
            //console.log(`Cancel: taskId ${taskId}`);

            let localeAddress = await AsyncStorage.getItem(this.state.fileName);

            if(localeAddress !== null)
            {
                await RNFetchBlob.fs.unlink(localeAddress).then(async () => {
                    await AsyncStorage.removeItem(this.state.fileName);
                    await this.setState({
                        hasLocale:false,
                        source : this.selectedFile.download_url
                    });
                });
            }

        });
    }
    async deleteFile(){
        let localeAddress = await AsyncStorage.getItem(this.state.fileName);
        RNFetchBlob.fs.unlink(localeAddress).then(() => {
            AsyncStorage.removeItem(this.state.fileName);
            this.setState({
                hasLocale:false,
                source : this.selectedFile.download_url
            });
        });
    }

    async addToCart(){

        let cart = this.props.app.cart;

        let product = this.props.product;

        product.count = 1;
        product.downloadable = this.state.downloadable;

        cart.push(product);

        await AsyncStorage.setItem("cart",JSON.stringify(cart));

        await this.props.setApp({
            ...this.props.app,
            cart
        });
        await this.setState({
            count:1
        })
    }
    async deleteFromCart(id){

        let cart = this.props.app.cart;

        let index = cart.findIndex(a => a.id === id);

        cart.splice(index,1);

        if(cart.length === 0){
            await AsyncStorage.removeItem("cart");
        }
        else {
            await AsyncStorage.setItem("cart",JSON.stringify(cart));
        }

        await this.props.setApp({
            ...this.props.app,
            cart
        });
        await this.setState({
            count:0
        })
    }
    async updateCart(data,index){

        let cart = this.props.app.cart;

        cart[index].count = parseInt(Helpers.ToEnglishNumber(data[0]));

        await AsyncStorage.setItem("cart",JSON.stringify(cart));

        await this.props.setApp({
            ...this.props.app,
            cart
        });
        await this.setState({
            count:parseInt(Helpers.ToEnglishNumber(data[0]))
        })

    }
    async showPicker(id) {
        let data = [];

        let cart = this.props.app.cart;
        let index = cart.findIndex(a => a.id === id);
        let product = cart[index];

        if(this.state.downloadable || (product.price === '' || product.price === '0' || product.sale_price === '0'))
        {
            data = ['۱'];
        }
        else {
            data = ['۱','۲','۳','۴','۵'];
        }

        Picker.init({
            pickerData: data,
            pickerConfirmBtnText:'تایید',
            pickerCancelBtnText:'لغو',
            pickerTitleText:'تعداد را انتخاب کنید',
            pickerConfirmBtnColor:[51, 153, 102,1],
            pickerCancelBtnColor:[218, 68, 83,1],
            pickerToolBarBg:[245, 244, 232,1],
            pickerFontFamily:'Vazir',
            pickerFontSize:18,
            selectedValue: [Helpers.ToPersianNumber(this.props.app.cart[index].count.toString())],
            onPickerConfirm: async data => {
                await this.updateCart(data,index);
            }
        });
        Picker.show();
    }

    async addFavorite(id){

        await this.setState({
            showLoadingModal:true
        });

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("product_id", id);

            let response = await fetch(this.props.global.baseApiUrl + '/product/favpadd',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    showLoadingModal:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    let favorite = [];

                    json.data.map(item => {
                        item.id = parseInt(item.id);
                        favorite.push(item);
                    });

                    await this.props.setApp({
                        ...this.props.app,
                        favorite:favorite
                    });
                    await this.setState({
                        showLoadingModal:false
                    });
                }
                else {
                    await this.setState({
                        showLoadingModal:false
                    });
                }
            }

        } catch (error) {
            //console.log(error);
            await this.setState({
                showLoadingModal:false
            });
        }
    }
    async deleteFavorite(id){
        await this.setState({
            showLoadingModal:true
        });

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("product_id", id);

            let response = await fetch(this.props.global.baseApiUrl + '/product/favpdel',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    showLoadingModal:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    if(json.data === "It's deleted")
                    {
                        await this.props.setApp({
                            ...this.props.app,
                            favorite:[]
                        });
                    }
                    else {
                        let favorite = [];

                        json.data.map(item => {
                            item.id = parseInt(item.id);
                            favorite.push(item);
                        });

                        await this.props.setApp({
                            ...this.props.app,
                            favorite:favorite
                        });
                    }
                    await this.setState({
                        showLoadingModal:false
                    });
                }
                else {
                    await this.setState({
                        showLoadingModal:false
                    });
                }
            }

        } catch (error) {
            //console.log(error);
            await this.setState({
                showLoadingModal:false
            });
        }
    }

    async getProductRequest(){
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            if(this.props.user.apiToken !== null){
                formData.append("usertoken", this.props.user.apiToken);
            }

            formData.append("product_id", this.props.product.id);

            let response = await fetch(this.props.global.baseApiUrl + '/product/getpbyid',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    loading:false
                });
            }
            else {

                let json = await response.json();

                //console.log(json);

                if(json.success){

                    let regex = /(<([^>]+)>)/ig;
                    //let desc = json.data.desc !== null ? json.data.desc.replace(regex, '') : 'توضیحی ثبت نشده است.';
                    let desc = json.data.desc !== null ? "<div style='font-family: Vazir;font-size:12px;direction: rtl;text-align: right;'>" + json.data.desc + "</div>" : "<div style='font-family: Vazir;font-size:12px;direction: rtl;text-align: right;'>توضیحی ثبت نشده است!</div>";
                    //console.log(desc);

                    await this.setState({
                        downloadable:json.data.is_downlodable,
                        files:json.data.download_url,
                        isBuy:json.data.is_buy,
                        desc,
                        loading:false
                    });
                }
                else {
                    await this.setState({
                        loading:false
                    });
                }
            }


        } catch (error) {
            console.log(error);
            await this.setState({
                loading:false
            });
        }
        //console.log(this.state);
    }

    renderImage(prop){
        let width = prop.width && prop.width !== null ? parseInt(prop.width) : 0;
        let height = prop.height && prop.height !== null ? parseInt(prop.height) : 0;
        //console.log(width,height);
        return(
            <Image key={12} source={{uri : prop.src}} style={[styles.renderedImage,{width:width}]} />
        );
    }

    render() {
        //console.log(this.images);

        let product = this.props.product;

        const dot = <View style={styles.dot} />;
        const activeDot = <View style={styles.activeDotContainer} >
            {/*<View style={styles.activeDotInner} />*/}
        </View>;

        let ModalContent = () => {
            if(!this.state.isBuy){
                return(
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Icon name="md-information-circle" style={{color:'#fd5459',fontSize:50}} />
                        <Text style={[styles.fileModalText,{marginTop:10}]}>
                            برای مشاهده لطفا این محصول را خریداری کنید.
                        </Text>
                    </View>
                );
            }
            else if (this.state.hasLocale) {
                return (
                    <View style={styles.fileModalRow}>
                        <View style={[styles.fileModalColumn, {borderLeftWidth: 0}]}>
                            <TouchableOpacity onPress={this.deleteFile} style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="ios-trash" style={styles.fileModalIcon}/>
                                <Text style={styles.fileModalText}>حذف از حافظه</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.fileModalColumn}>
                            <TouchableOpacity onPress={this.view} style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="ios-cloud-done" style={styles.fileModalIcon}/>
                                <Text style={styles.fileModalText}>مشاهده آفلاین</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                );
            }
            else {

                let LocaleSection = () => {
                    if (this.state.downloading) {
                        return (
                            <View>
                                <Text style={[styles.fileModalText, {marginBottom: 10}]}>در حال دریافت</Text>
                                {this.state.totalSize > 0 && <Progress.Bar progress={this.state.downloadPercent} color={this.props.global.grColorTwo} borderColor="#dddddd" width={100}/>}
                                <Text style={[styles.fileModalText, {fontSize: 10, color: this.props.global.grColorTwo}]}>{Helpers.ToPersianNumber(this.state.downloadedSize.toString())} مگابایت</Text>
                                <TouchableOpacity onPress={this.cancelDownload} style={{justifyContent: 'center', alignItems: 'center'}} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                                    <Text style={[styles.fileModalText, {marginTop: 20, fontSize: 10, color: '#fd5459'}]}>لغو دانلود</Text>
                                </TouchableOpacity>
                            </View>
                        );
                    }
                    else {
                        return (
                            <TouchableOpacity onPress={this.downloadFile} style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="ios-cloud-download" style={styles.fileModalIcon}/>
                                <Text style={styles.fileModalText}>دانلود</Text>
                            </TouchableOpacity>
                        );
                    }
                };

                return (
                    <View style={styles.fileModalRow}>
                        <View style={[styles.fileModalColumn, {borderLeftWidth: 0}]}>
                            <LocaleSection/>
                        </View>
                        <View style={styles.fileModalColumn}>
                            <TouchableOpacity onPress={this.view} style={{justifyContent: 'center', alignItems: 'center'}}>
                                <Icon name="logo-youtube" style={styles.fileModalIcon}/>
                                <Text style={styles.fileModalText}>مشاهده آنلاین</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                );
            }
        };

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode="cover" style={styles.mainBackground} source={require("./../../assets/images/shared/mainbg.jpg")} />

                <Header style={styles.header}>
                    <Left style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>
                        <BackButton/>
                    </Left>
                    <Right style={{flex:2,alignItems:'center',justifyContent:'flex-end'}}>
                        {/*<HeaderRightLabel title={product.title}/>*/}
                        <SearchButton/>
                        <BasketButton/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="dark-content" />

                <Content style={{flex:1,backgroundColor:'#ffffff'}}>
                    {product.image !== null && <Swiper style={styles.productSliderContainer} showsButtons={false} loop={false} activeDot={activeDot} dot={dot}
                            paginationStyle={{marginBottom: -20}}>
                        {/*<View style={styles.productSliderSlide} key={0}>*/}
                            {/*<Image style={styles.productSliderImage} source={{uri:product.image + '?ref =' + this.newDate}}/>*/}
                        {/*</View>*/}
                        {this.images.map((item,index) => {
                            return(
                                <View key={index} style={styles.productSliderSlide}>
                                    <Image style={styles.productSliderImage} source={{uri:item + '?ref =' + this.newDate}}/>
                                </View>
                            );
                        })}
                    </Swiper>}
                    <View style={styles.productTitleContainer}>
                        <Text style={styles.productTitleTextOne} numberOfLines={2}>{product.title}</Text>
                    </View>
                    <View style={styles.productTitleContainer}>
                        <Text style={[styles.productTitleTextOne,{color:'#999999',fontSize:14}]} numberOfLines={2}>رنگ بندی های موجود :</Text>
                        <View style={styles.productColorContainer}>
                            <TouchableOpacity style={styles.productColorItem}>
                                <View style={[styles.productColorItemColor,{backgroundColor:'#D4AF37'}]} />
                                <Text style={styles.productColorItemText}>طلایی</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.productColorItem}>
                                <View style={[styles.productColorItemColor,{backgroundColor:'#C0C0C0'}]} />
                                <Text style={styles.productColorItemText}>نقره ای</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.productColorItem}>
                                <View style={[styles.productColorItemColor,{backgroundColor:'#000000',borderColor:'#2490ff'}]} />
                                <Text style={[styles.productColorItemText,{color:'#2490ff'}]}>مشکی</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.productDescContainer}>
                        <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                            <Text style={[styles.productTitleTextOne,{color:'#777777',fontSize:12}]} numberOfLines={2}>گارانتی ۱۸ ماهه سی تلکام </Text>
                            <Icon name='md-ribbon' style={{marginLeft:5,fontSize:24}} />
                        </View>

                        {this.state.desc !== null && <View style={{flexDirection:'row',width:'100%',justifyContent:'space-between',alignItems:'center',padding:10,backgroundColor:'#f9faff',borderRadius: 10,marginTop:10}}>
                            <View>
                                <Icon style={{color:'#2490ff'}} name='ios-arrow-back'/>
                            </View>
                            <View style={{flex:1,justifyContent:'flex-end',width:'100%',paddingRight:10}}>
                                <Text style={[styles.productDescInfoTitle]}>
                                    مشخصات فنی
                                </Text>
                            </View>
                            <View>
                                <Icon style={{color:'#aaaaaa'}} name='md-paper'/>
                            </View>
                        </View>}

                        {/*این html viewer زیر رو توی یه صفحه دیگه نشون بده*/}
                        {/*{this.state.desc !== null && <View style={styles.productDescInfoContainer}>*/}
                            {/*<Text style={styles.productDescInfoTitle}>*/}
                                {/*توضیحات :*/}
                            {/*</Text>*/}
                            {/*/!*<ViewMoreText*!/*/}
                                {/*/!*numberOfLines={6}*!/*/}
                                {/*/!*renderViewMore={this.renderViewMore}*!/*/}
                                {/*/!*renderViewLess={this.renderViewLess}*!/*/}
                                {/*/!*textStyle={styles.productDescInfoText}*!/*/}
                            {/*/!*>*!/*/}
                                {/*/!*<Text>{this.state.desc}</Text>*!/*/}
                            {/*/!*</ViewMoreText>*!/*/}
                            {/*<HTML html={this.state.desc}*/}
                                  {/*tagsStyles={{*/}
                                      {/*p : {justifyContent:Platform.OS === 'ios' ? 'flex-end' : 'flex-start',textAlign:Platform.OS === 'ios' ? 'justify' : 'right',writingDirection:'rtl'} ,*/}
                                      {/*div : {textAlign:Platform.OS === 'ios' ? 'justify' : 'right',writingDirection:'rtl'}*/}
                                  {/*}}*/}
                                  {/*//imagesMaxWidth={200} imagesInitialDimensions={{width:200,height:200}}*/}
                                  {/*renderers = {{*/}
                                      {/*img: this.renderImage*/}
                                  {/*}}*/}
                            {/*/>*/}
                        {/*</View>}*/}
                    </View>
                </Content>

                <View style={styles.productBuySection}>
                    <View style={styles.productBuySectionLeft}>
                        {this.props.app.favorite.findIndex(a => a.id === product.id) === -1 && <TouchableOpacity activeOpacity={.7} style={styles.productFavoriteButton} onPress={() => this.addFavorite(product.id)}>
                            <Icon name='ios-heart' style={[styles.productFavoriteButtonIcon,{color:'#ffffff'}]}/>
                        </TouchableOpacity>}
                        {this.props.app.favorite.findIndex(a => a.id === product.id) > -1 && <TouchableOpacity activeOpacity={.7} style={styles.productFavoriteButton} onPress={() => this.deleteFavorite(product.id)}>
                            <Icon name='ios-heart' style={[styles.productFavoriteButtonIcon,{color:'#ff4141'}]}/>
                        </TouchableOpacity>}
                    </View>

                    <View style={styles.productBuySectionCenter}>
                        <View style={styles.productBuySectionCenterInner}>
                            {this.state.loading && <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                <ActivityIndicator color={this.props.global.grColorTwo} size='large' />
                            </View>}

                            {(this.state.isBuy !== null && !this.state.isBuy && !this.state.loading) && <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',flex:1}}>
                                {(product.sale_price !== '0' && product.price !== '' && product.price !== '0') && <NumberFormat value={product.sale_price !== '' ? product.sale_price : product.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                                    <Text style={[styles.productPrice,{marginTop:0,color:'#ffffff'}]}>{Helpers.ToPersianNumber(value)} تومان</Text>
                                }/>}
                                {(product.price === '' || product.price === '0' || product.sale_price === '0') && <Text style={[styles.productPrice,{marginTop:0,color:'#ffffff'}]}>رایگان</Text>}
                                {(product.sale_price !== '') && <NumberFormat value={product.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                                    <Text style={[styles.productPrice,{color: 'rgba(255,255,255,.4)',textDecorationLine:'line-through',fontSize:14,marginLeft:10}]}>{Helpers.ToPersianNumber(value)}</Text>
                                }/>}
                            </View>}
                            {(this.state.downloadable !== null && !this.state.loading) && <View>

                                {(this.state.count === 0 && ((!this.state.isBuy && this.state.downloadable) || !this.state.downloadable)) && <TouchableOpacity style={styles.productBuySectionCenterInnerButton} activeOpacity={.8} onPress={this.addToCart}>
                                    <Icon name='md-add-circle' style={{color:'#fff',fontSize:20}}/>
                                </TouchableOpacity>}

                                {this.state.count > 0 && <TouchableOpacity style={styles.productBuySectionCenterInnerButton} activeOpacity={.8} onPress={() => this.deleteFromCart(product.id)}>
                                    <Icon name='md-remove-circle' style={{color:'#fff',fontSize:20}}/>
                                </TouchableOpacity>}

                            </View>}
                        </View>
                    </View>

                    {this.state.count > 0 && <View style={styles.productBuySectionRight}>
                        <TouchableOpacity style={{width:40,height:40,borderRadius:5,borderWidth:3/PixelRatio.get(),borderColor:'#eeeeee',justifyContent:'center',alignItems:'center'}} onPress={() => this.showPicker(product.id)} activeOpacity={.7}>
                            <Text style={[styles.cartListItemCenterCount,{fontSize:20,marginTop:0}]} numberOfLines={1}>{Helpers.ToPersianNumber(this.state.count.toString())}</Text>
                        </TouchableOpacity>
                    </View>}
                </View>

                <LoadingModal show={this.state.showLoadingModal} />

                <Modal isVisible={this.state.showFileModal} backdropOpacity={0.3} animationIn="fadeIn" animationOut="fadeOut"
                       onBackdropPress={() => this.setState({showFileModal: false})} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                    <View style={styles.fileModal}>
                        <View style={styles.fileModalContainer}>
                            <TouchableOpacity onPress={() => this.setState({ showFileModal : false })} style={{position:'absolute',top:3,left:5,zIndex:999999}}>
                                <Icon name="ios-close-circle" style={{color:'#555555',fontSize:20}} />
                            </TouchableOpacity>
                            <ModalContent/>
                        </View>
                    </View>
                </Modal>

            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setApp : app => {
            dispatch(setApp(app))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Product);