import React from 'react';
import Svg,{ G, Path,Ellipse,Circle} from 'react-native-svg';

export default class OtherRefrenceIcon extends React.Component {
    render() {
        return (
            <Svg height="20" width="20" viewBox="0 0 64 64">
                <Path fill='#b7c4cb' d="M64,16L64,16L32,0L0.1,16H0v32l32,16l32-16V16L64,16z M32,3.9l24.1,12.1l-9.2,4.6l-24.2-12L32,3.9z
	 M18.5,10.8L43,22.6l-11,5.3L7.9,16L18.5,10.8z M4,20.1h1.3H4V18l26,13v28L4,46V20.1z M60,22v24L34,59V31l26-13V22z"/>
            </Svg>
        );
    }
}