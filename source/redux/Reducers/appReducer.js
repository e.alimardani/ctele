import {SET_APP} from "../Actions/Type";

const initialState = {
    categories: [],
    cart:[],
    favorite:[]
};

export default app = (state = initialState, action = {}) => {
    switch (action.type)
    {
        case SET_APP:
            const { app } = action;
            return {
                categories: app.categories,
                cart: app.cart,
                favorite:app.favorite
            };
            break;
        default:
            return state;
    }
}