import React from 'react';
import Svg,{ G, Path,Ellipse,Circle} from 'react-native-svg';

export default class ProfileAddressIcon extends React.Component {
    render() {
        return (
            <Svg height="20" width="20" viewBox="0 0 64 64">
                <Path fill='#b7c4cb' d="M42,8L22,0L2,8v56l20-8l20,8l20-8V0L42,8L42,8z M20,52L6,57.4v-46L20,6V52L20,52z M40,58l-16-6V6l16,6V58
				L40,58z M58,52.7l-14,5.4v-46l14-5.4V52.7L58,52.7z"/>
            </Svg>
        );
    }
}