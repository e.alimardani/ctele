import React from 'react';
import { TouchableOpacity,View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import MenuIcon from "../../assets/icon/MenuIcon";

export default class MenuButton extends React.Component {

    render() {
        return (

            <View style={{padding: 5, paddingTop: 7}}>
                <TouchableOpacity onPress={() => Actions.drawerOpen()} activeOpacity={0.7} hitSlop={{top: 10, left: 10, bottom: 10, right: 10}}>
                    <MenuIcon/>
                </TouchableOpacity>
            </View>

        )
    }
}
