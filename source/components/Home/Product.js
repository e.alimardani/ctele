import React from 'react';
import {
    StatusBar,
    Image,
    TextInput,
    ActivityIndicator,
    Text,
    RefreshControl,
    TouchableOpacity,
    View,
    FlatList,
    Dimensions
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import MenuButton from "../Shared/MenuButton";
import BasketButton from "../Shared/BasketButton";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import Helpers from "../Shared/Helper";
import NumberFormat from "react-number-format";
import Swiper from "react-native-swiper";
//import { nav} from 'react-navigation';

class Product extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            sliders:[],
            sliderLoading:true,
            products:[],
            productsLoading:true,
            refreshing: false,
            LoadingCategory:true
        };

        this.newDate = props.global.newDate;

        this.getSlidersRequest = this.getSlidersRequest.bind(this);
        this.getProducts = this.getProducts.bind(this);
        this.onRefresh = this.onRefresh.bind(this);

        this.search = this.search.bind(this);

    }

    async componentWillMount(){
        await this.getSlidersRequest();
        await this.getProducts();
    }

    async getSlidersRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                sliderLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/view/allsliders',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    sliderLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        sliders:json.data,
                        sliderLoading:false
                    });
                }
                else {
                    await this.setState({
                        sliderLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                sliderLoading:false
            });
        }
    }

    async getProducts(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                productsLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", '10');
            formData.append("offset", '1');
            formData.append("pcatid", 40);

            let response = await fetch(this.props.global.baseApiUrl + '/product/getproductbycat',
                {
                    method: "POST",
                    body: formData
                });

           

            if (response.status !== 200) {
                await this.setState({
                    productsLoading:false,
                    LoadingCategory:false
                });
            }
            else {

                let json = await response.json();
                //console.log(json);

                if(json.success){

                    await this.setState({
                        products:json.data,
                        productsLoading:false,
                        LoadingCategory:false
                    });
                }
                else {
                    await this.setState({
                        productsLoading:false,
                        LoadingCategory:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                productsLoading:false
            });
        }
    }
    renderItemNew({ item,index }) {
        return (
            <TouchableOpacity style={[styles.productHorizontalSectionListItem,{marginRight:index === 0 ? 15 : 15,marginLeft:index + 1 === this.state.products.length ? 15 : 0}]} key={item.id} activeOpacity={.8}
                              onPress={() => Actions.push('product',{product:item})}>
                <View style={styles.productHorizontalSectionListItemContainer}>
                    {item.image !== null && <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={{uri:item.image + '?ref =' + this.newDate}}/>}
                    {item.image === null && <Image resizeMode='contain' style={styles.productHorizontalSectionListItemImage} source={require('./../../assets/images/shared/product.png')}/>}
                    {/*{item.oldPrice !== '' && <View style={styles.productHorizontalSectionListItemPercent}>*/}
                    {/*<Text style={styles.productHorizontalSectionListItemPercentText}>{item.percent}</Text>*/}
                    {/*</View>}*/}
                </View>
                <Text style={styles.productHorizontalSectionListItemName} numberOfLines={2}>{item.title}</Text>
                {(item.sale_price !== '') && <NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                    <Text style={styles.productHorizontalSectionListItemOldPrice}>{Helpers.ToPersianNumber(value)} تومان</Text>
                }/>}
                {(item.sale_price !== '0' && item.price !== '' && item.price !== '0') && <NumberFormat value={item.sale_price !== '' ? item.sale_price : item.price} displayType={'text'} thousandSeparator={true} renderText={value =>
                    <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:item.sale_price !== '' ? 10 : 0}]}>{Helpers.ToPersianNumber(value)} تومان</Text>
                }/>}
                {(item.price === '' || item.price === '0' || item.sale_price === '0') && <Text style={[styles.productHorizontalSectionListItemPrice,{marginTop:10}]}>رایگان</Text>}
            </TouchableOpacity>
        );
    }

    async onRefresh(){
        await this.setState({
            sliders:[],
            sliderLoading:true,
            products:[],
            productsLoading:true,
            refreshing: true
        });
        await this.getSlidersRequest();
        await this.getProducts();
        await this.setState({
            refreshing: false
        });
    }

    async search(){
        //console.log(this.searchBox._lastNativeText);
        this.searchBox.blur();
        Actions.push('productSearch',{query:this.searchBox._lastNativeText});
    }

    ShowCategory(){
        if((this.props.app.categories.length === 0) && (!this.state.LoadingCategory)){
            return(<Text style={{marginTop:20,fontSize:18,textAlign:'center',fontFamily:'$YekanBold'}}>دسته بندی ای وجود ندارد !</Text>)
        }
        else if(this.state.LoadingCategory){
            return(<ActivityIndicator style={{marginTop:20}} size="large" color='#2490ff'/>)
        }
        else{
            return(
                <View style={[styles.brandContainer,{marginBottom:25,justifyContent:'space-around'}]}>
                   {(this.props.app.categories[0]) &&
                    <View>
                        {(this.props.app.categories[0].cat_parent==0) &&
                        <TouchableOpacity style={styles.brandItem} activeOpacity={.8} onPress={() => Actions.push('products',{title:this.props.app.categories[0].cat_name,type:'category',categoryId:this.props.app.categories[0].cat_id})}>
                        {(this.props.app.categories[0].image_cat==null) && <Image style={styles.brandItemImage} source={require('./../../assets/images/product/phonelogo.jpg')} />}
                        {(this.props.app.categories[0].image_cat!=null) && <Image style={styles.brandItemImage} source={{uri: this.props.app.categories[0].image_cat}} />}
                        <Text style={[styles.productHorizontalSectionHeaderText,{fontSize:13}]}>{this.props.app.categories[0].cat_name}</Text>
                        </TouchableOpacity>}
                    </View>
                    }
                    {(this.props.app.categories[1]) &&
                    <View>
                        {(this.props.app.categories[1].cat_parent==0) &&
                        <TouchableOpacity style={styles.brandItem} activeOpacity={.8} onPress={() => Actions.push('products',{title:this.props.app.categories[1].cat_name,type:'category',categoryId:this.props.app.categories[1].cat_id})}>
                            {(this.props.app.categories[1].image_cat==null) && <Image style={styles.brandItemImage} source={require('./../../assets/images/product/samlogo.jpg')} />}
                            {(this.props.app.categories[1].image_cat!=null) && <Image style={styles.brandItemImage} source={{uri: this.props.app.categories[1].image_cat}} />}
                            <Text style={[styles.productHorizontalSectionHeaderText,{fontSize:13}]}>{this.props.app.categories[1].cat_name}</Text>
                        </TouchableOpacity>}
                    </View>
                    }
                    {(this.props.app.categories[2]) &&
                    <View>
                        {(this.props.app.categories[1].cat_parent==0) &&
                        <TouchableOpacity style={styles.brandItem} activeOpacity={.8} onPress={() => Actions.push('products',{title:this.props.app.categories[2].cat_name,type:'category',categoryId:this.props.app.categories[2].cat_id})}>
                            {(this.props.app.categories[2].image_cat==null) && <Image style={styles.brandItemImage} source={require('./../../assets/images/product/applelogo.png')} />}
                            {(this.props.app.categories[2].image_cat!=null) && <Image style={styles.brandItemImage} source={{uri: this.props.app.categories[2].image_cat}} />}
                            <Text style={[styles.productHorizontalSectionHeaderText,{fontSize:13}]}>{this.props.app.categories[2].cat_name}</Text>
                        </TouchableOpacity>}
                    </View>
                        }

                </View>
            )
        }
       
        
    }

    render() {

        const dot = <View style={styles.dot} />;
        const activeDot = <View style={styles.activeDotContainer} >
            <View style={styles.activeDotInner} />
        </View>;

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode="cover" style={styles.mainBackground} source={require("./../../assets/images/shared/mainbg.jpg")} />

                <Header style={styles.header}>
                    <Left style={{flex:0.2,alignItems:'center',justifyContent:'center'}}>
                        <BasketButton/>
                    </Left>
                    <Body style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                        <Image style={styles.headerImage} source={require('./../../assets/images/shared/logo.png')}/>
                    {/*<HeaderRightLabel title='محصولات'/>*/}
                    </Body>
                    <Right style={{flex:.2,alignItems:'center',justifyContent:'center'}}>
                        <MenuButton/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="dark-content" />

                <Content refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} tintColor='#ffffff'/>} >
                    <View style={styles.searchBoxContainer}>
                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.searchBoxInput}
                                   placeholderTextColor="#cccccc" onSubmitEditing={this.search} returnKeyType='search'
                                   placeholder='عبارت مورد نظر خود را تایپ کنید' ref={ref => this.searchBox = ref}/>

                        <TouchableOpacity style={styles.searchBoxButton} onPress={this.search}>
                            <Icon name="ios-search" style={styles.searchBoxIcon}/>
                        </TouchableOpacity>
                    </View>

                    {(!this.state.sliderLoading && this.state.sliders.length > 0) && <Swiper style={styles.postSliderContainer} showsButtons={false} loop={false} activeDot={activeDot} dot={dot}
                                                                                             paginationStyle={{marginBottom: -20}}>
                        {this.state.sliders.map((item,index) => {
                            return(
                                <TouchableOpacity activeOpacity={.9} key={index} style={styles.postSliderSlide}>
                                    <Image style={styles.postSliderImage} source={{uri:item.post.image + '?ref =' + this.newDate}}/>
                                    {/*<View style={styles.postSliderTitleContainer}>*/}
                                        {/*<Text numberOfLines={1} style={styles.postSliderTitleText}>{item.post.title}</Text>*/}
                                    {/*</View>*/}
                                </TouchableOpacity>
                            );
                        })}
                    </Swiper>}
                    {this.state.sliderLoading && <View style={{ height : Dimensions.get('window').height * .30 , backgroundColor:'#ffffff' , justifyContent:'center',alignItems:'center' }}>
                        <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                    </View>}

                    <View style={[styles.productHorizontalSection,{paddingVertical:10}]}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>برندها</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}}
                                              //onPress={() => Actions.drawerOpen()}
                                              onPress={() => Actions.drawerOpen()}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه برندها</Text>
                            </TouchableOpacity>
                        </View>
                        {this.ShowCategory()}
                    </View>
                    <TouchableOpacity style={{paddingHorizontal:20,backgroundColor:'#ffffff',justifyContent:'center',alignItems:'center'}} activeOpacity={.9} onPress={() =>this.props.props.navigation.navigate('Special')}>
                        <Image resizeMode='contain' source={require('./../../assets/images/home/productBanner.png')} style={styles.homeBanner} />
                    </TouchableOpacity>
                    {(this.state.products.length > 0 && !this.state.productsLoading) && <View style={styles.productHorizontalSection}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>گوشی های آیفون</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}} onPress={() => Actions.push('products',{title:'گوشی های آیفون',type:'category',categoryId:40})}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.state.products}
                            renderItem={this.renderItemNew.bind(this)}
                            keyExtractor={(item) => item.id.toString()}
                            style={styles.productHorizontalSectionList}
                            horizontal={true}
                            inverted={true}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>}
                    {(this.state.productsLoading) && <View style={styles.productHorizontalSection}>
                        <View style={styles.productHorizontalSectionHeader}>
                            <Text style={styles.productHorizontalSectionHeaderText}>جدیدترین محصولات</Text>
                            <TouchableOpacity activeOpacity={.7} style={{justifyContent:'center',alignItems:'center',flexDirection: 'row'}} onPress={() => Actions.push('products',{title:'جدیدترین محصولات',type:'newproduct'})}>
                                <Icon name="ios-arrow-back" style={styles.productHorizontalSectionHeaderIcon}/>
                                <Text style={styles.productHorizontalSectionHeaderMoreText}>نمایش همه</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{height:150,justifyContent:'center',alignItems:'center'}}>
                            <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                        </View>
                    </View>}

                </Content>
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global,
        app:state.app
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Product);