import React from 'react';
import {
    StatusBar,
    Image,
    TextInput,
    ActivityIndicator,
    Text,
    RefreshControl,
    TouchableOpacity,
    View,
    FlatList,
    Dimensions, ImageBackground
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body, Accordion} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import MenuButton from "../Shared/MenuButton";
import BasketButton from "../Shared/BasketButton";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import Helpers from "../Shared/Helper";
import Swiper from "react-native-swiper";
import moment from 'jalali-moment';
import NumberFormat from "react-number-format";
import Modal from "react-native-modal";
import LinearGradient from "react-native-linear-gradient";

class Blog extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            refreshing: false,
            sliders:[],
            sliderLoading:true,
            posts:[],
            postLoading:true,
            videos:[],
            videoLoading:true,
            categories:[],
            categoryLoading:true,
            showModal:false
        };

        this.newDate = props.global.newDate;

        this.search = this.search.bind(this);
        this.onRefresh = this.onRefresh.bind(this);
        this.getSlidersRequest = this.getSlidersRequest.bind(this);
        this.getPostsRequest = this.getPostsRequest.bind(this);
        this.getCategoriesRequest = this.getCategoriesRequest.bind(this);
        this.getVideosRequest = this.getVideosRequest.bind(this);

    }

    async componentWillMount(){
        await this.getCategoriesRequest();
        await this.getSlidersRequest();
        await this.getPostsRequest();
        await this.getVideosRequest();
    }

    async getSlidersRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                sliderLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/view/allsliders',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    sliderLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        sliders:json.data,
                        sliderLoading:false
                    });
                }
                else {
                    await this.setState({
                        sliderLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                sliderLoading:false
            });
        }
    }

    async getPostsRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                postLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", 5);
            formData.append("offset", 1);

            let response = await fetch(this.props.global.baseApiUrl + '/post/all',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    postLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        posts:json.data,
                        postLoading:false
                    });
                }
                else {
                    await this.setState({
                        postLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                postLoading:false
            });
        }
    }
    renderPostItem({ item,index }) {
        if(index === 0) return null;
        return (
            <TouchableOpacity style={[styles.singlePost,{margin:0,width : Dimensions.get('window').width * .6,marginRight:index === 1 ? 20 : 10 ,marginLeft:index + 1 === this.state.posts.length ? 20 : 0}]} key={item.id} activeOpacity={.8}
                              onPress={() => Actions.push('post' , {title:item.title,url:item.permalink})}>
                <View style={[styles.singlePostTop,{height:Dimensions.get('window').height * .15}]}>
                    <Image style={styles.singlePostImage} source={{uri: item.image + '?ref =' + this.newDate}}/>
                    <View style={styles.singlePostCategory}>
                        <Text style={styles.singlePostCategoryText}>{item.categories[0].category_name}</Text>
                    </View>
                </View>
                <View style={styles.singlePostBottom}>
                    <Text numberOfLines={1} style={[styles.singlePostTitle,{fontSize:12}]}>{item.title}</Text>
                    <Text numberOfLines={1} style={styles.singlePostDate}>{Helpers.ToPersianNumber(moment(item.create_date, 'YYYY-MM-DD HH:mm').locale('fa').format('DD MMMM YYYY | HH:mm'))}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    async getVideosRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                videoLoading:false
            });
        }

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("perpage", 5);
            formData.append("offset", 1);

            let response = await fetch(this.props.global.baseApiUrl + '/view/allvideos',
                {
                    method: "POST",
                    body: formData
                });

            if (response.status !== 200) {
                await this.setState({
                    videoLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){

                    await this.setState({
                        videos:json.data,
                        videoLoading:false
                    });
                }
                else {
                    await this.setState({
                        videoLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                videoLoading:false
            });
        }
    }
    renderVideoItem({ item,index }) {
        if(index === 0) return null;
        return (
            <TouchableOpacity style={[styles.singleVideo,{margin:0,width : Dimensions.get('window').width * .6,height:Dimensions.get('window').height * .2,marginRight:index === 1 ? 20 : 10 ,marginLeft:index + 1 === this.state.videos.length ? 20 : 0}]}
                              key={item.id} activeOpacity={.8} onPress={() => Actions.push('video',{video:item})}>
                <ImageBackground resizeMode='cover' style={styles.singleVideoImage} source={{uri: item.image + '?ref =' + this.newDate}}>
                    <View style={styles.singleVideoTop}>
                        <View style={[styles.singleVideoPlay,{width:40,height:40,borderRadius:20}]}>
                            <Icon style={[styles.singleVideoPlayIcon,{fontSize:30}]} name='ios-play'/>
                        </View>
                    </View>
                    <View style={styles.singleVideoBottom}>
                        <LinearGradient colors={['rgba(0,0,0,0)','rgba(0,0,0,.9)']} style={styles.grButtonGradient}>
                            <Text numberOfLines={1} style={[styles.singleVideoTitle,{fontSize:12}]}>{item.title}</Text>
                        </LinearGradient>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        );
    }

    async getCategoriesRequest(){
        if (await Helpers.CheckNetInfo()) {
            await this.setState({
                categoryLoading:false
            });
        }

        try {
            await this.setState({
                categoryLoading:true
            });

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);

            let response = await fetch(this.props.global.baseApiUrl + '/post/getpostcats',
                {
                    method: "POST",
                    body: formData
                });

            //console.log(response);

            if (response.status !== 200) {
                await this.setState({
                    categoryLoading:false
                });
            }
            else {

                let json = await response.json();

                if(json.success){
                    await this.setState({
                        categories:json.data,
                        categoryLoading:false
                    });
                }
                else {
                    await this.setState({
                        categoryLoading:false
                    });
                }
            }
        } catch (error) {
            await this.setState({
                categoryLoading:false
            });
        }
    }

    async onRefresh(){
        await this.setState({
            sliders:[],
            sliderLoading:true,
            posts:[],
            postLoading:true,
            videos:[],
            videoLoading:true,
            refreshing: true
        });
        await this.getSlidersRequest();
        await this.getPostsRequest();
        await this.getVideosRequest();
        await this.setState({
            refreshing: false
        });
    }

    async search(){
        //console.log(this.searchBox._lastNativeText);
        this.searchBox.blur();
        Actions.push('productSearch',{query:this.searchBox._lastNativeText});
    }

    renderHeader(item,expanded) {
        if(this.state.categories.filter(a => a.parent === item.id).length > 0){
            return (
                <View style={[styles.drawerTitleRow,{borderBottomColor : expanded ? '#ffffff' : '#f4f7f9'}]}>
                    <Text style={expanded ? styles.drawerTitleRowTextExpanded : styles.drawerTitleRowText}>
                        {item.category_name}
                    </Text>
                    {expanded
                        ? <Icon style={styles.drawerTitleRowExpandedIcon} name="md-remove-circle" />
                        : <Icon style={styles.drawerTitleRowUnexpandedIcon} name="md-add-circle" />}
                </View>
            );
        }
        else {
            return (
                <TouchableOpacity style={[styles.drawerTitleRow,{borderBottomColor : expanded ? '#ffffff' : '#f4f7f9'}]}
                                  activeOpacity={.7} onPress={async() => { await this.setState({showModal:false});Actions.push('posts',{title:item.category_name,type:'category',categoryId:item.id}) } }>
                    <Text style={expanded ? styles.drawerTitleRowTextExpanded : styles.drawerTitleRowText}>
                        {item.category_name}
                    </Text>
                    {expanded
                        ? <Icon style={styles.drawerTitleRowExpandedIcon} name="md-remove-circle" />
                        : <Icon style={styles.drawerTitleRowUnexpandedIcon} name="md-add-circle" />}
                </TouchableOpacity>
            );
        }
    }
    renderContent(item) {
        return(
            <View style={styles.drawerContent}>
                <TouchableOpacity style={[styles.drawerContentRow,{borderBottomColor : '#ffffff' }]}
                                  activeOpacity={.7} onPress={async() => { await this.setState({showModal:false});Actions.push('posts',{title:item.category_name,type:'category',categoryId:item.id}) } }>
                    <Icon name='ios-arrow-back' style={styles.drawerContentIcon} />
                    <Text style={styles.drawerContentText}>{item.category_name}</Text>
                </TouchableOpacity>
                {this.state.categories.filter(a => a.parent === item.id).map((item2,index) => {
                    return(
                        <TouchableOpacity key={item2.id} style={[styles.drawerContentRow,{borderBottomColor : index + 1 === this.state.categories.filter(a => a.parent === item.id).length  ? '#ffffff' : '#f4f7f9' }]}
                                          activeOpacity={.7} onPress={async() => { await this.setState({showModal:false});Actions.push('posts',{title:item2.category_name,type:'category',categoryId:item2.id}) } }>
                            <Icon name='ios-arrow-back' style={styles.drawerContentIcon} />
                            <Text style={styles.drawerContentText}>{item2.category_name}</Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        )
    }

    render() {

        const dot = <View style={styles.dot} />;
        const activeDot = <View style={styles.activeDotContainer} >
            <View style={styles.activeDotInner} />
        </View>;

        let firstPost = this.state.posts.length > 0 ? this.state.posts[0] : null;
        let firstVideo = this.state.videos.length > 0 ? this.state.videos[0] : null;

        return (
            <Container style={styles.mainContainer}>
                <Image resizeMode="cover" style={styles.mainBackground} source={require("./../../assets/images/shared/mainbg.jpg")} />

                <Header style={styles.header}>
                    <Left style={{flex:0.2,alignItems:'center',justifyContent:'center'}}>
                        <BasketButton/>
                    </Left>
                    <Body style={{flex:1,alignItems:'flex-end',justifyContent:'center'}}>
                    {/*<Image style={styles.headerImage} source={require('./../../assets/images/splash/Splash-Logo.png')}/>*/}
                    <HeaderRightLabel title='بلاگ'/>
                    </Body>
                    <Right style={{flex:.2,alignItems:'center',justifyContent:'center'}}>
                        <MenuButton/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="dark-content" />

                <Content refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} tintColor='#ffffff'/>} >
                    <View style={styles.searchBoxContainer}>
                        <TextInput underlineColorAndroid="rgba(0,0,0,0)" style={styles.searchBoxInput}
                                   placeholderTextColor="#cccccc" onSubmitEditing={this.search} returnKeyType='search'
                                   placeholder='عبارت مورد نظر خود را تایپ کنید' ref={ref => this.searchBox = ref}/>

                        <TouchableOpacity style={styles.searchBoxButton} onPress={this.search}>
                            <Icon name="ios-search" style={styles.searchBoxIcon}/>
                        </TouchableOpacity>
                    </View>

                    {(!this.state.sliderLoading && this.state.sliders.length > 0) && <Swiper style={styles.postSliderContainer} showsButtons={false} loop={false} activeDot={activeDot} dot={dot}
                                                                                             paginationStyle={{marginBottom: -20}}>
                        {this.state.sliders.map((item,index) => {
                            return(
                                <TouchableOpacity activeOpacity={.9} key={index} style={styles.postSliderSlide} onPress={() => Actions.push('post' , {title:item.post.title,url:item.post.permalink})}>
                                    <Image style={styles.postSliderImage} source={{uri:item.post.image + '?ref =' + this.newDate}}/>
                                    <View style={styles.postSliderTitleContainer}>
                                        <Text numberOfLines={1} style={styles.postSliderTitleText}>{item.post.title}</Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        })}
                    </Swiper>}

                    {this.state.sliderLoading && <View style={{ height : Dimensions.get('window').height * .30 , backgroundColor:'#ffffff' , justifyContent:'center',alignItems:'center' }}>
                        <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                    </View>}

                    {(!this.state.postLoading && this.state.posts.length > 0) && <View style={styles.blogContainer}>

                        <View style={styles.blogHeader}>
                            <TouchableOpacity style={styles.blogHeaderButton} activeOpacity={.8} onPress={() => this.setState({showModal:true})}>
                                <Text style={styles.blogHeaderButtonText}>
                                    دسته بندی
                                </Text>
                            </TouchableOpacity>
                            <Text style={styles.blogHeaderText}>
                                مقالات
                            </Text>
                        </View>

                        <TouchableOpacity activeOpacity={.8} style={styles.singlePost} onPress={() => Actions.push('post' , {title:firstPost.title,url:firstPost.permalink})}>
                            <View style={styles.singlePostTop}>
                                <Image style={styles.singlePostImage} source={{uri: firstPost.image + '?ref =' + this.newDate}}/>
                                <View style={styles.singlePostCategory}>
                                    <Text style={styles.singlePostCategoryText}>{firstPost.categories[0].category_name}</Text>
                                </View>
                            </View>
                            <View style={styles.singlePostBottom}>
                                <Text numberOfLines={1} style={styles.singlePostTitle}>{firstPost.title}</Text>
                                <Text numberOfLines={1} style={styles.singlePostDate}>{Helpers.ToPersianNumber(moment(firstPost.create_date, 'YYYY-MM-DD HH:mm').locale('fa').format('DD MMMM YYYY | HH:mm'))}</Text>
                            </View>
                        </TouchableOpacity>

                        <FlatList
                            data={this.state.posts}
                            renderItem={this.renderPostItem.bind(this)}
                            keyExtractor={(item) => item.id.toString()}
                            style={{backgroundColor:'transparent'}}
                            horizontal={true}
                            inverted={true}
                            showsHorizontalScrollIndicator={false}
                            removeClippedSubviews={true}
                        />

                        <TouchableOpacity activeOpacity={.7} style={styles.postMoreButton} onPress={() => Actions.push('posts' , {title:'مقالات'})}>
                            <Icon name='ios-arrow-back' style={{fontSize:20,color:'#393939',marginRight:10}}/>
                            <Text style={styles.postMoreButtonText}>نمایش بیشتر</Text>
                        </TouchableOpacity>

                    </View>}

                    {this.state.postLoading && <View style={{ height : Dimensions.get('window').height * .2 , backgroundColor:'#f4f7f9' , justifyContent:'center',alignItems:'center' }}>
                        <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                    </View>}

                    {(!this.state.videoLoading && this.state.videos.length > 0) && <View style={[styles.blogContainer,{backgroundColor:'#222328'}]}>

                        <View style={styles.videoHeader}>
                            <Text style={styles.videoHeaderText}>
                                ویدئو ها
                            </Text>
                        </View>

                        <TouchableOpacity activeOpacity={.9} style={styles.singleVideo} onPress={() => Actions.push('video',{video:firstVideo})}>
                            <ImageBackground resizeMode='cover' style={styles.singleVideoImage} source={{uri: firstVideo.image + '?ref =' + this.newDate}}>
                                <View style={styles.singleVideoTop}>
                                    <View style={styles.singleVideoPlay}>
                                        <Icon style={styles.singleVideoPlayIcon} name='ios-play'/>
                                    </View>
                                </View>
                                <View style={styles.singleVideoBottom}>
                                    <LinearGradient colors={['rgba(0,0,0,0)','rgba(0,0,0,.9)']} style={styles.grButtonGradient}>
                                        <Text numberOfLines={2} style={styles.singleVideoTitle}>{firstVideo.title}</Text>
                                    </LinearGradient>
                                </View>
                            </ImageBackground>
                        </TouchableOpacity>

                        <FlatList
                            data={this.state.videos}
                            renderItem={this.renderVideoItem.bind(this)}
                            keyExtractor={(item) => item.id.toString()}
                            style={{backgroundColor:'transparent'}}
                            horizontal={true}
                            inverted={true}
                            showsHorizontalScrollIndicator={false}
                            removeClippedSubviews={true}
                        />

                        <TouchableOpacity activeOpacity={.7} style={styles.videoMoreButton} onPress={() => Actions.push('videos')}>
                            <Icon name='ios-arrow-back' style={{fontSize:20,color:'#ffffff',marginRight:10}}/>
                            <Text style={styles.videoMoreButtonText}>نمایش بیشتر</Text>
                        </TouchableOpacity>

                    </View>}

                    {this.state.videoLoading && <View style={{ height : Dimensions.get('window').height * .2 , backgroundColor:'#222328' , justifyContent:'center',alignItems:'center' }}>
                        <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                    </View>}

                </Content>

                <Modal isVisible={this.state.showModal} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut" backdropColor='#000000'
                       onBackdropPress={() => this.setState({showModal: false})} onBackButtonPress={() => this.setState({showModal: false})}>
                    <View style={styles.requestModalContainer} >
                        <TouchableOpacity onPress={() => this.setState({showModal: false})} style={{position: 'absolute', top: 10, left: 20, zIndex: 999999}}>
                            <Icon name="ios-close" style={{color: '#555555', fontSize: 50}}/>
                        </TouchableOpacity>
                        <Container style={{width:'100%',height:'100%'}}>
                            <View style={styles.drawerHeader}>
                                <Text style={styles.drawerHeaderText}>دسته بندی بلاگ</Text>
                            </View>
                            {(!this.state.categoryLoading && this.state.categories.length > 0) && <View style={{flex:1,backgroundColor:'#fff'}}>
                                <Content style={{backgroundColor:'transparent'}}>
                                    <Accordion
                                        dataArray={this.state.categories.filter(a => a.parent === 0)}
                                        renderHeader={this.renderHeader.bind(this)}
                                        renderContent={this.renderContent.bind(this)}
                                        style={{borderWidth:0}}
                                    />
                                </Content>
                            </View>}

                            {this.state.categoryLoading && <View style={{flex:1,backgroundColor:'#fff',justifyContent: 'center',alignItems: 'center'}}>
                                <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                            </View>}

                            {(!this.state.categoryLoading && this.state.categories.length === 0) && <View style={{flex:1,backgroundColor:'#fff',justifyContent: 'center',alignItems: 'center'}}>
                                <Text style={styles.drawerTitleRowText}>دسته بندی وجود ندارد!</Text>
                            </View>}

                        </Container>
                    </View>
                </Modal>
            </Container>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Blog);