import React from 'react';
import {
    StatusBar,
    Image,
    TextInput,
    AsyncStorage,
    Text,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
    PixelRatio,
    FlatList,
    ActivityIndicator
} from 'react-native';
import {Header, Icon, Left, Right, Content, Container, Body,Form,Item,Label,Input} from 'native-base';
import styles from './Style';
import { Actions } from 'react-native-router-flux';
import {setUser} from "../../redux/Actions/Index";
import {connect} from "react-redux";
import MenuButton from "../Shared/MenuButton";
import BasketButton from "../Shared/BasketButton";
import HeaderRightLabel from "../Shared/HeaderRightLabel";
import Helpers from "../Shared/Helper";
import moment from 'jalali-moment';
import Helper from '../../assets/infra/Helper'
import Modal from 'react-native-modal';
//import ReserveService from '../Services/ReserveService'

class Special extends React.Component {

    constructor(props) {
        let dateCount = [];

        for(let i = 0;i<=31;i++){
            dateCount.push(i);
        }

        super(props);
        this.state={
            dates:dateCount,
            date:null,
            dataTime:[],
            times: [],
            LoadingTime:false,
            isData:true,
            service:null,
            staffs:null,
            selectTime:[],
            selected:null,
            selectedEnd:'',
            showCategoryModal:false,
            showServiceModal:false,
            servicesLoading:false,
            categoriesLoading:false,
            wrapperLoading:false,
            categories:[],
            tempcat:[],
            tempServ:[],
            Services:[],
            catSelectedId:null,
            catSelectedName:"",
            servSelectedId:null,
            servSelectedName:"",
            mail:'',
            phone:'',
            message:'',
            ShowDataModal:false,
            ShowDoneModal:false,
            messageMain:'',
            fullName:''
        }
        Date.prototype.addDays = function(days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        }
    }

    renderTimeItem({ item , index }){

        if(item.status){
            return(
                <TouchableOpacity onPress={() => this.setState({selectTime:item, selected:item.start , selectedEnd:item.end})} style={ [styles.specialDetailDayItem,{backgroundColor:(this.state.selected!==item.start) ? '#f6f6f6' : '#2490ff'}]}>
                    <Text style={ [styles.specialDetailDayText,{color:(this.state.selected!==item.start) ?'#999999' : '#ffffff'}]}>{Helper.ToPersianNumber(item.start.substr(0,5))} تا {Helper.ToPersianNumber(item.end.substr(0,5))}</Text>
                </TouchableOpacity>
            )
        }
        else if(!item.status){
            return(
                <TouchableOpacity onPress={() => this.setState({selectTime:item, selected:item.start , selectedEnd:item.end})} style={ [styles.specialDetailDayItem,{backgroundColor:(this.state.selected!==item.start) ? '#f6f6f6' : '#2490ff'}]}>
                    <Text style={ [styles.specialDetailDayText,{color:(this.state.selected!==item.start) ?'#999999' : '#ffffff'}]}>{Helper.ToPersianNumber(item.start.substr(0,5))} تا {Helper.ToPersianNumber(item.end.substr(0,5))}</Text>
                </TouchableOpacity>
            )
        }
    }


    renderDateItem({ item }) {

        let date = new Date().addDays(item);
        let dateString = moment(date).format('YYYY-MM-DD');
        let selected = this.state.date !== null && dateString === this.state.date;
    
        return (

            <TouchableOpacity style={[styles.specialDetailDayItem,{backgroundColor:selected ? '#2490ff' : '#f6f6f6'}]}
            onPress={() => this.setState({ date : dateString },()=>this.openModalDate())}
            >
                <Text style={[styles.specialDetailDayText,{color:selected ? '#ffffff' : '#999999'}]}>{moment(date).locale('fa').format('dddd')}</Text>
                <Text style={[styles.specialDetailDayNumber,{color:selected ? '#ffffff' : '#999999'}]}>{Helper.ToPersianNumber(moment(date).format('jDD'))}</Text>
                <Text style={[styles.specialDetailDayMonth,{color:selected ? '#ffffff' : '#999999'}]}>{moment(date).locale('fa').format('MMMM')}</Text>
            </TouchableOpacity>
        );
    }

    setIsData(data){
        if(data === 0){
            this.setState({isData:false})
        }
        else{
            this.setState({isData:true})
        }
    }
    async GetTimes(){

        try {

            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("service_id", this.state.servSelectedId);
            formData.append("staff_id", this.state.catSelectedId);
            formData.append("fromdate", this.state.date);
            formData.append("todate", this.state.date);
            formData.append("fromtime", "00:00:00");
            formData.append("totime", "23:45:00");


            let response = await fetch(this.props.global.baseApiUrlUSER + '/customer/gettimes',
                {
                    method: "POST",
                    body: formData,
                });
                this.setState({dataTime:JSON.parse(response._bodyInit)})
                this.setState({times:this.state.dataTime.data[0] , LoadingTime:false}) 
                this.setIsData(this.state.dataTime.data.length)

        } catch (error) {
            console.log(error)         
        }

    }
    ShowTimes(){
        if(this.state.LoadingTime){
            return(
                <ActivityIndicator color='#2490ff' size="large"/>
            )
        }
        else if(!this.state.LoadingTime){
            
            if(this.state.isData){
                return(
                    <FlatList
                        data={this.state.times}
                        renderItem={this.renderTimeItem.bind(this)}
                        keyExtractor={(item) => item.start.toString()}
                        style={{backgroundColor:'#ffffff',maxHeight:20}}
                        horizontal={true}
                        inverted={true}
                        showsHorizontalScrollIndicator={false}
                    />
                )
            }
            else {
                return( <Text style={{fontFamily:'Vazir'}}>در این تاریخ زمانی وجود ندارد</Text> )
            }
        }

    }
    renderCategoryItem({ item }) {
        return (
            <TouchableOpacity style={{padding:15,borderBottomColor:'#eaeaea',borderBottomWidth:0.7}} onPress={()=>this.setState({catSelectedId:item.id , catSelectedName:item.name , showCategoryModal:false})}>
                <Text style={{fontFamily:'Vazir',fontSize:15}}>{item.name}</Text>
            </TouchableOpacity>
        );
    }

    renderServiceItem({ item }) {
        return (
            <TouchableOpacity style={{padding:15,borderBottomColor:'#eaeaea',borderBottomWidth:0.7}} onPress={()=>this.setState({servSelectedId:item.id , servSelectedName:item.title , showServiceModal:false})}>
            <Text style={{fontFamily:'Vazir',fontSize:15}}>{item.title}</Text>
        </TouchableOpacity>
        );
    }

    async getcat(){
        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            let response = await fetch(this.props.global.baseApiUrlUSER + '/service/getcategory',
                {
                    method: "POST",
                    body: formData,
                });
                this.setState({tempcat:JSON.parse(response._bodyInit)})
                this.setState({categories:this.state.tempcat.data , categoriesLoading:false})


        } catch (error) {
            console.log(error)         
        }
    }

    openModalDate(){
        
        if(this.state.catSelectedId === null){
            this.setState({
                messageMain:'ابتدا یک دسته بندی انتخاب کنید'
            })
        }
        else if(this.state.servSelectedId === null){
            this.setState({
                messageMain:'ابتدا یک  سرویس انتخاب کنید'
            })
        }

        else{
             this.setState({ LoadingTime:true, messageMain:'' },()=>this.GetTimes())
        }
    }
    openModalservice(){
        
        if(this.state.catSelectedId === null){
            this.setState({
                messageMain:'ابتدا یک دسته بندی انتخاب کنید'
            })
        }
        else{
            this.setState({showServiceModal:true , servicesLoading:true,messageMain:'',date:null },()=>this.getserv())
        }
    }


    async getserv(){

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("cat_id", this.state.catSelectedId);

            let response = await fetch(this.props.global.baseApiUrlUSER + '/service/getservicebycat',
                {
                    method: "POST",
                    body: formData,
                });
                this.setState({tempServ:JSON.parse(response._bodyInit)})
                this.setState({Services:this.state.tempServ.data , servicesLoading:false})

        } catch (error) {
            console.log(error)         
        }
    }

    checkModal(){

        if(this.state.catSelectedId === null){
            this.setState({messageMain: 'لطفا یک دسته بندی انتخاب کنید'})
        }
        else if(this.state.servSelectedId === null){
            this.setState({messageMain: 'لطفا یک سرویس انتخاب کنید'})
        }
        else if(this.state.date === null){
            this.setState({messageMain: 'لطفا یک تاریخ انتخاب کنید'})
        }
        else if(this.state.selected === null){
            this.setState({messageMain: 'لطفا یک زمان انتخاب کنید'})
        }
        else{
            this.setState({wrapperLoading:true,messageMain:''},()=> this.NextStep())
        }
    }

    async AddReserve(){

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("fullname", this.props.user.fullName);
            formData.append("email", this.props.user.email);
            formData.append("phone_number", this.props.user.phoneNumber);
            formData.append("service_id", this.state.service);
            formData.append("staff_id", this.state.catSelectedId);
            formData.append("fromdate", this.state.date);
            formData.append("todate",  this.state.date);
            formData.append("fromtime", this.state.selected);
            formData.append("totime", this.state.selectedEnd);


            let response = await fetch(this.props.global.baseApiUrlUSER + '/customer/addreserve',
                {
                    method: "POST",
                    body: formData,
                });
                if(response.status == 200){
                    this.setState({ShowDoneModal:true,wrapperLoading:false},()=>this.clearData())
                }

        } catch (error) {
            console.log(error)         
        }
    }

    checkModalData(){

        if(this.state.fullName === ''){
            this.setState({message: 'لطفا نام و نام‌خانوادگی را وارد کنید'})
        }
        else if(this.state.phone === ''){
            this.setState({message: 'لطفا شماره‌همراه را وارد کنید'})
        }
        else if(this.state.mail === ''){
            this.setState({message: 'لطفا پست الکترونیکی را وارد کنید'})
        }
        else{
            this.reserve()
        }
    }

    clearData(){
        this.setState({
            service: null,
            staffs: null,
            selected:null,
            fullName:'',
            phone:'',
            mail:'',
            date:null,
            servSelectedName:'',
            catSelectedName:'',
            catSelectedId:null,
            servSelectedId:null,
            messageMain:'',
            times:[]
        })
    }

    async reserve(){

        try {
            let formData = new FormData();
            formData.append("admintoken", this.props.global.adminToken);
            formData.append("usertoken", this.props.user.apiToken);
            formData.append("fullname", (!this.props.user.fullName) ? this.state.fullName :this.props.user.fullName);
            formData.append("email", (this.props.user.email === null) ? this.state.mail :this.props.user.email);
            formData.append("phone_number", (this.props.user.phoneNumber === null) ? this.state.phone :this.props.user.phoneNumber);
            formData.append("service_id", this.state.servSelectedId);
            formData.append("staff_id", this.state.catSelectedId);
            formData.append("fromdate", this.state.date);
            formData.append("todate", this.state.date);
            formData.append("fromtime", this.state.selected);
            formData.append("totime", this.state.selectedEnd);


            let response = await fetch(this.props.global.baseApiUrlUSER + '/customer/addreserve',
                {
                    method: "POST",
                    body: formData,
                });
                this.setState({RESPONSE:JSON.parse(response._bodyInit) ,wrapperLoading:false},()=>this.Done(response))

        } catch (error) {
            console.log(error)         
        }
    }

    async NextStep(){
        
        if(this.props.user.apiToken === null ){
            this.setState({ShowDataModal:true})
        }

        else{
                try {
                    let formData = new FormData();
                    formData.append("admintoken", this.props.global.adminToken);
                    formData.append("usertoken", this.props.user.apiToken);
                    formData.append("fullname", this.props.user.fullName);
                    formData.append("email", this.props.user.email);
                    formData.append("phone_number", this.props.user.phoneNumber);
                    formData.append("service_id", this.state.servSelectedId);
                    formData.append("staff_id", this.state.catSelectedId);
                    formData.append("fromdate", this.state.date);
                    formData.append("todate",  this.state.date);
                    formData.append("fromtime", this.state.selected);
                    formData.append("totime", this.state.selectedEnd);
    
                    let response = await fetch(this.props.global.baseApiUrlUSER + '/customer/addreserve',
                        {
                            method: "POST",
                            body: formData,
                        });
                        if(response.status == 200){
                            this.setState({ShowDoneModal:true,wrapperLoading:false},()=>this.clearData())
                        }
        
                } catch (error) {
                    console.log(error)         
                }
                this.setState({
                    message:''})
            }

    }

    async Done(response){

        let json = await response.json();
        if(!json.success){
            this.setState({message:json.messages[0]})
        }
        else if(json.success){
            this.setState({RES:JSON.parse(response._bodyInit)})

            await this.props.setUser({
                    name: this.props.user.first_name,
                    family: this.props.user.last_name,
                    fullName: this.state.fullName,
                    phoneNumber: this.state.phone,
                    apiToken: this.state.RES.usertoken,
                    id: this.state.RES.user_id,
                    email: this.state.RES.data.user_email
                });

            await AsyncStorage.setItem("apiToken", this.state.RES.usertoken)

            this.setState({ShowDataModal: false,message:'' , ShowDoneModal:true},()=>{this.setState({ShowDoneModal:true}) , this.clearData()})
        }
    }

    render() {

        return (
            <Container style={styles.mainContainer}>
            <View style={{flex:1}}>
                <Image resizeMode="cover" style={styles.mainBackground} source={require("./../../assets/images/shared/mainbg.jpg")} />

                <Header style={styles.header}>
                    <Left style={{flex:0.2,alignItems:'center',justifyContent:'center'}}>
                        <BasketButton/>
                    </Left>
                    <Body style={{flex:1,alignItems:'flex-end',justifyContent:'center'}}>
                        <HeaderRightLabel title='خدمات ویژه'/>
                    </Body>
                    <Right style={{flex:.2,alignItems:'center',justifyContent:'center'}}>
                        <MenuButton/>
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="dark-content" />

                <View style={{backgroundColor:'#f4f7f9',flex:1}}>
                    <View style={styles.specialContainer}>
                        <TouchableOpacity activeOpacity={.7} onPress={()=>this.setState({showCategoryModal:true , categoriesLoading:true },()=>{ this.getcat() , this.clearData()})} style={styles.specialItem}>
                            <View style={styles.specialItemLabel}>
                                <Text style={styles.specialItemLabelText}>دسته بندی</Text>
                            </View>
                            <Icon style={styles.specialItemIcon} name='ios-arrow-back'/>
                            <Text style={styles.specialItemText}>{this.state.catSelectedName}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={.7} onPress={()=> this.openModalservice()} style={[styles.specialItem,{marginTop:20}]}>
                            <View style={styles.specialItemLabel}>
                                <Text style={styles.specialItemLabelText}>نام سرویس</Text>
                            </View>
                            <Icon style={styles.specialItemIcon} name='ios-arrow-back'/>
                            <Text style={styles.specialItemText}>{this.state.servSelectedName}</Text>
                        </TouchableOpacity>

                        <View  style={[styles.specialDetail,{marginTop:20}]}>
                            <View style={styles.specialDetailLabel}>
                                <Text style={styles.specialDetailLabelText}>تاریخ رزرو</Text>
                            </View>

                            <View style={{paddingVertical:15,borderBottomWidth:3/PixelRatio.get(),borderBottomColor:'#eeeeee'}}>
                                <FlatList
                                    data={this.state.dates}
                                    renderItem={this.renderDateItem.bind(this)}
                                    keyExtractor={(item) => item.toString()}
                                    style={{backgroundColor:'#ffffff',maxHeight:70}}
                                    horizontal={true}
                                    inverted={true}
                                    showsHorizontalScrollIndicator={false}
                                />
                            </View>

                            <View style={{paddingVertical:15}}>
                                {this.ShowTimes()}
                            </View>

                        </View>
                        {this.state.wrapperLoading && <ActivityIndicator style={{marginTop:10}} color="#2490ff" size="large" />}
                        {(!this.state.wrapperLoading) && <TouchableOpacity style={styles.specialButton} onPress={()=>this.checkModal()}>
                            <Icon style={styles.specialButtonIcon} name='ios-arrow-back'/>
                            <Text style={styles.specialButtonText}>مرحله بعد</Text>
                        </TouchableOpacity>}
                        
                        <Text style={{textAlign:'center',fontFamily:'Vazir' ,color:'red',paddingTop:10}}>{this.state.messageMain}</Text>

                    </View>
                </View>
                </View>
                {/*Category Modal*/}
                <Modal isVisible={this.state.showCategoryModal} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut" backdropColor='#000000'
                    onBackdropPress={() => this.setState({ showCategoryModal: false })} onBackButtonPress={() => this.setState({ showCategoryModal: false })}>
                    <View style={styles.requestModalContainer} >
                        <TouchableOpacity onPress={() => this.setState({ showCategoryModal: false })} style={{ position: 'absolute', top: 10, left: 20, zIndex: 999999 }}>
                            <Icon name="ios-close" style={{ color: '#555555', fontSize: 50 }} />
                        </TouchableOpacity>

                        <Container style={{ width: '100%', height: '100%' }}>
                            <View style={styles.drawerHeader}>
                                <Text style={[styles.drawerHeaderText,{color:'#2490ff'}]}>دسته بندی ها</Text>
                            </View>
                            
                            {(!this.state.categoriesLoading && this.state.categories.length > 0) && <View style={{ flex: 1, backgroundColor: '#fff' }}>
                                <FlatList
                                    data={this.state.categories}
                                    renderItem={this.renderCategoryItem.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            </View>}
                            {this.state.categoriesLoading && <View style={{ flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator color='#2490ff' size='large' />
                            </View>}

                            {(!this.state.categoriesLoading && this.state.categories.length === 0) && <View style={{ flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.drawerTitleRowText}>دسته بندی وجود ندارد!</Text>
                            </View>}

                        </Container>
                    </View>
                </Modal>

                {/*Service Modal*/}
                <Modal isVisible={this.state.showServiceModal} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut" backdropColor='#000000'
                    onBackdropPress={() => this.setState({ showServiceModal: false })} onBackButtonPress={() => this.setState({ showServiceModal: false })}>
                    <View style={styles.requestModalContainer} >
                        <TouchableOpacity onPress={() => this.setState({ showServiceModal: false })} style={{ position: 'absolute', top: 10, left: 20, zIndex: 999999 }}>
                            <Icon name="ios-close" style={{ color: '#555555', fontSize: 50 }} />
                        </TouchableOpacity>
                        <Container style={{ width: '100%', height: '100%' }}>
                            <View style={styles.drawerHeader}>
                                <Text style={[styles.drawerHeaderText,{color:'#2490ff'}]}>سرویس ها</Text>
                            </View>

                            {(!this.state.servicesLoading && this.state.Services.length > 0) && <View style={{ flex: 1, backgroundColor: '#fff' }}>
                                <FlatList
                                    data={this.state.Services}
                                    renderItem={this.renderServiceItem.bind(this)}
                                    keyExtractor={(item) => item.id.toString()}
                                    style={{ backgroundColor: 'transparent' }}
                                />
                            </View>}

                            {this.state.servicesLoading && <View style={{ flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator color={this.props.global.grColorTwo} size='large' />
                            </View>}

                            {(!this.state.servicesLoading && this.state.Services.length === 0) && <View style={{ flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.drawerTitleRowText}>سرویسی وجود ندارد!</Text>
                            </View>}

                        </Container>
                    </View>
                </Modal>

                {/*add data Modal*/}
                <Modal style={{margin:0,marginTop:180,marginBottom:0}} isVisible={this.state.ShowDataModal} backdropColor='#aaaa' backdropOpacity={0.8}
                    //swipeDirection='down' onSwipeComplete={() => this.setState({ ShowModalReserve: false })} 
                    onBackdropPress={() => this.setState({ ShowDataModal: false, wrapperLoading:false })} onBackButtonPress={() => this.setState({ ShowDataModal: false , wrapperLoading:false })}>
                    <Container style={[styles.Modal,{paddingVertical:10}]}>
                        
                        <View style={{flexDirection:'row-reverse',justifyContent:'space-between', borderBottomColor:'#eaeaea',borderBottomWidth:1,paddingBottom:10}}>
                            <Text style={{fontFamily:'VazirBold',color:'#2490ff',fontSize:16,marginRight:15,alignSelf:'flex-end'}}>مشخصات فردی</Text>
                            <Text style={{fontFamily:'VazirBold',color:'#2490ff',fontSize:16,marginLeft:15,alignSelf:'flex-start'}}>مرحله ۲</Text>
                        </View>
                        
                        <Content style={styles.WrapperModal}>
                            <Form>
                                <View style={{marginVertical:15}}>
                                    <Text style={{flex:1,fontFamily:'Vazir',textAlign:'right',justifyContent:'flex-end',right:55,backgroundColor:'white',paddingHorizontal:10,zIndex:999,position:'absolute',color:'#aaa'}}>نام‌ و نام خانوادگی</Text>
                                    <View style={[styles.formRow,{marginTop:10}]} >
                                        <Input value={this.state.fullName} style={styles.textInput} onChangeText={(text)=>this.setState({fullName:text})}/>
                                    </View>
                                </View>
                                
                                <View style={{marginBottom:15}}>
                                    <Text style={{flex:1,fontFamily:'Vazir',textAlign:'right',justifyContent:'flex-end',right:55,backgroundColor:'white',paddingHorizontal:10,zIndex:999,position:'absolute',color:'#aaa'}}>شماره‌همراه (رمزعبور)</Text>
                                    <View style={[styles.formRow,{marginTop:10}]} >
                                        <Input maxLength={11} keyboardType="numeric" value={this.state.phone} style={styles.textInput} onChangeText={(text)=>this.setState({phone:text})}/>
                                    </View>
                                </View>

                                <View>
                                    <Text style={{flex:1,fontFamily:'Vazir',textAlign:'right',justifyContent:'flex-end',right:55,backgroundColor:'white',paddingHorizontal:10,zIndex:999,position:'absolute',color:'#aaa'}}>پست‌الکترونیکی</Text>
                                    <View style={[styles.formRow,{marginTop:10}]} >
                                        <Input keyboardType="email-address" value={this.state.mail} style={styles.textInput} onChangeText={(text)=>this.setState({mail:text})}/>
                                    </View>
                                </View>
                            </Form>
                            <View>
                                <Text style={{color:'red', fontFamily:'Vazir',textAlign:'center',paddingVertical:10}}>{this.state.message}</Text>
                            </View>

                            <View style={{flexDirection:'row-reverse',justifyContent:'space-around',alignItems:'center'}}>
                                <TouchableOpacity onPress={()=>{this.setState({ShowDataModal:false})}} style={[styles.submitBtn,{backgroundColor:'#eaeaea'}]} activeOpacity={0}>
                                    <Text style={{fontFamily:'Vazir' ,color:'#2490ff', fontSize:16,textAlign:'center'}}>مرحله قبل</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>{this.checkModalData()}} style={[styles.submitBtn,{flexDirection:'row',alignItems:'center'}]} activeOpacity={0}>
                                    <Icon style={styles.specialButtonIcon} name='ios-arrow-back'/>
                                    <Text style={{fontFamily:'Vazir' ,color:'white', fontSize:16,textAlign:'center'}}>مرحله بعد</Text>
                                </TouchableOpacity>
                            </View>

                        </Content>
                    </Container> 
                </Modal>

                {/*Done Modal*/}
                <Modal isVisible={this.state.ShowDoneModal} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut" backdropColor='#000000'
                    onBackdropPress={() => this.setState({ ShowDoneModal: false })} onBackButtonPress={() => this.setState({ ShowDoneModal: false })}>
                    <View style={{width:200,height:120,alignSelf:'center'}} >
                        <Container style={{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'space-around',borderRadius:10}}>           
                            <Text style={{fontFamily:'Vazir',fontSize:16 ,color:'green'}}>با موفقیت انجام شد</Text>
                            <TouchableOpacity onPress={()=>this.setState({ShowDoneModal:false})} style={{paddingHorizontal:15,paddingBottom:5,backgroundColor:'#2490ff',borderRadius:20}}>
                                <Text style={{fontFamily:'Vazir',fontSize:18,color:'white'}}>تایید</Text>
                            </TouchableOpacity>
                        </Container>
                    </View>
                </Modal>
            </Container>
            
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => {
            dispatch(setUser(user))
        }
    }
};


const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Special);