import React from 'react';
import { View, Text, TouchableOpacity, StatusBar, ActivityIndicator} from 'react-native';
import {Icon} from 'native-base';
import {styles} from './Style'
import Modal from 'react-native-modal';
import {connect} from "react-redux";

class LoadingModal extends React.Component {

    render() {

        return (

            <Modal isVisible={this.props.show} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut" backdropColor='#000000'>
                <View style={styles.loadingModalContainer}>
                    <View>
                        <ActivityIndicator color={this.props.global.grColorTwo} size="large"/>
                    </View>
                </View>
            </Modal>

        )
    }
}

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,null)(LoadingModal);