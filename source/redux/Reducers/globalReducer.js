
export default global = (state = {}, action = {}) => {
    return {
        baseApiUrl: 'http://ctelecom.ir/api/v1',
        baseApiUrlUSER: 'http://ctelecom.wpfix.ir/bookly/v1',
        adminToken: '29b0fbd121b264864e933b9884a1883304553b98b8b8da5809a9a5e912c061ba',
        grColorOne:'#2490ff',
        grColorTwo:'#0066cc',
        newDate:new Date().toISOString(),
        website:'http://ctelecom.ir',
        appName:'سی تلکام',
        slogan:'با پکیج های تخصصی ما سلامتی خود را تضمین کنید'
    };
}