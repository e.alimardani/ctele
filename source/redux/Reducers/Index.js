import { combineReducers } from 'redux';
import user from './userReducer';
import global from './globalReducer';
import app from './appReducer';

export default combineReducers({
    user,global,app
});