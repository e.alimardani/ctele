import RequestManager from "../lib/RequestManager";
import Helpers from "../lib/Helper";

export default ReserveService = {
    GetCategoriesList: async (source) => {
        if (await Helpers.CheckNetInfo()) {
            await source.setState({
                categoriesLoading: false
            });
            return false;
        }

        await source.setState({
            categoriesLoading: true
        });

        let formData = new FormData();
        console.log("ddddddddddddddddddd")
        const response = await RequestManager.Post({
            source: source,
            url: "/service/getcategory",
            data: formData,
            showLoading: false
        });

        let newState = {
            categoriesLoading: false
        }
        console.log(response)
        if (response != null)
            newState.categories = response.data;

        await source.setState(newState);
    },
    GetServicesList: async (source) => {
        console.log("hi" + source)
        if (await Helpers.CheckNetInfo()) {
            await source.setState({
                servicesLoading: false
            });
            return false;
        }
        await source.setState({
            servicesLoading: true
        });

        let formData = new FormData();
        formData.append("cat_id", source.state.category.id);
        //console.log("hi" +source.state.category)
        const response = await RequestManager.Post({
            source: source,
            url: "/service/getservicebycat",
            data: formData,
            showLoading: false
        });

        let newState = {
            servicesLoading: false
        }
        if (response != null)
            newState.services = response.data;

        await source.setState(newState);
    },
    GetStaffsList: async (source) => {
        if (await Helpers.CheckNetInfo()) {
            await source.setState({
                staffsLoading: false
            });
            return false;
        }
        await source.setState({
            staffsLoading: true
        });

        let formData = new FormData();
        formData.append("service_id", source.state.service.id);
        const response = await RequestManager.Post({
            source: source,
            url: "/staff/getstaffbyservice",
            data: formData,
            showLoading: false
        });

        let newState = {
            staffsLoading: false
        }
        if (response != null)
            newState.staffs = response.data;

        await source.setState(newState);
    },
    SetFromDate: (source, date) => {
        console.log(date);
    },
    SetFromTime: async (source, time) => {
        await source.setState({ showFromTimePicker: false });
        console.log(time);
    }
};