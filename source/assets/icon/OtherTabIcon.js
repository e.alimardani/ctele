import React from 'react';
import Svg,{ G, Path,Ellipse,Circle} from 'react-native-svg';

export default class OtherTabIcon extends React.Component {
    render() {
        return (
            <Svg height="25" width="25" viewBox="0 0 86.4 21.6">
                <Path fill={this.props.color} d="M43.2,0c-6,0-10.8,4.8-10.8,10.8s4.8,10.8,10.8,10.8c6,0,10.8-4.8,10.8-10.8
	C54,4.9,49.1,0,43.2,0z M75.6,0c-6,0-10.8,4.8-10.8,10.8c0,6,4.8,10.8,10.8,10.8c6,0,10.8-4.8,10.8-10.8c0,0,0,0,0,0
	C86.4,4.9,81.5,0,75.6,0L75.6,0z M10.8,0C4.8,0,0,4.8,0,10.8c0,6,4.8,10.8,10.8,10.8c6,0,10.8-4.8,10.8-10.8c0,0,0,0,0,0
	C21.6,4.9,16.7,0,10.8,0L10.8,0z"/>
            </Svg>
        );
    }
}