import React from 'react';
import { View, Text, TouchableOpacity, StatusBar, ActivityIndicator} from 'react-native';
import {Icon} from 'native-base';
import {styles} from './Style'
import Modal from 'react-native-modal';
import {connect} from "react-redux";

class RequestModal extends React.Component {

    render() {

        let Content = () => {
            if(this.props.loading)
            {
                return(
                    <View>
                        <ActivityIndicator color={this.props.global.grColorTwo} size='large'/>
                        <Text style={styles.requestModalText}>
                            لطفا کمی صبر کنید
                        </Text>
                    </View>
                );
            }
            else if(this.props.hasError)
            {
                return(
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Icon name="md-information-circle" style={{color:'#fd5459',fontSize:50}} />
                        <Text style={styles.requestModalText}>
                            {this.props.errorDesc}
                        </Text>
                    </View>
                );
            }
            return(
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Icon name="md-checkmark-circle" style={{color:'#2fcc71',fontSize:50}} />
                    <Text style={styles.requestModalText}>
                        {this.props.errorDesc}
                    </Text>
                </View>
            );
        };

        return (

            <Modal isVisible={this.props.showModal} backdropOpacity={0.5} animationIn="fadeIn" animationOut="fadeOut" backdropColor='#000000'
                   onBackdropPress={() => !this.props.loading && this.props.setter.setState({showModal: false})} onBackButtonPress={() => !this.props.loading && this.props.setter.setState({showModal: false})}>
                <View style={styles.requestModalContainer} >
                    {!this.props.loading && <TouchableOpacity onPress={() => this.props.setter.setState({showModal: false})} style={{position: 'absolute', top: 3, left: 5, zIndex: 999999}}>
                        <Icon name="ios-close-circle" style={{color: '#555555', fontSize: 20}}/>
                    </TouchableOpacity>}
                    <Content/>
                </View>
            </Modal>

        )
    }
}

const mapStateToProps =(state) =>  {
    return{
        user: state.user,
        global:state.global
    }
};

export default connect(mapStateToProps,null)(RequestModal);