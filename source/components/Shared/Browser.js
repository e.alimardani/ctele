import React from 'react';
import {ActivityIndicator, Text, View, StatusBar, WebView, Image} from 'react-native';

import { Container ,Header,Left,Body,Right} from 'native-base';
import {styles} from './Style';
import BackButton from "../Shared/BackButton";
import Helpers from "./Helper";

export default class Browser extends React.Component {

    constructor(props)
    {
        super(props);

        this.state = {
            url: '',
            loading: true
        };
    }

    async componentWillMount(){
        if(await Helpers.CheckNetInfo()){
            return;
        }
        this.setState({
            url: this.props.url,
        })
    }

    render() {


        return (
            <Container>
                <Header style={[styles.header,{overflow: 'hidden',zIndex:999}]} >
                    <Image resizeMode="cover" style={styles.mainBackground} source={require("./../../assets/images/shared/mainbg.jpg")} />
                    <Left style={{flex: .2,justifyContent:'center',alignItems:'flex-start'}}>
                        <BackButton/>
                    </Left>
                    <Body style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                        <Text style={[styles.notFoundText,{marginTop:0,color:'#292e71'}]}>{this.props.title}</Text>
                    </Body>
                    <Right style={{flex: .2,justifyContent:'center',alignItems:'flex-end'}} >
                        {this.state.loading && <ActivityIndicator color="#292e71"/>}
                    </Right>
                </Header>

                <StatusBar backgroundColor="transparent" translucent={true} barStyle="dark-content" />

                <WebView
                    source={{uri: this.state.url , method : 'GET' }}
                    onLoadStart={() => { this.setState({loading:true}) } }
                    onLoadEnd={() => { this.setState({loading:false}) } }
                    style={{zIndex:-1}}
                />
            </Container>
        )
    }
}
